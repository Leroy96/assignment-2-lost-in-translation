import { STORAGE_KEY_USER } from "const/storageKeys";
import React, { createContext, useContext, useState } from "react";
import { storageRead } from "utils/storage";

const UserContext = createContext(undefined)

export const useUser = () => {
    return useContext(UserContext)
}

// export default UserContext

// Provider
const UserProvider = ({ children }) => {
    const [user, setUser] = useState(storageRead(STORAGE_KEY_USER))

    const state = {
        user,
        setUser
    }


    return (
        <UserContext.Provider value={state}>
            {children}
        </UserContext.Provider>
    )
}

export default UserProvider