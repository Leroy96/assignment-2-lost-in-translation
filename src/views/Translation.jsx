import React, { useState } from 'react'
import withAuth from 'hoc/withAuth'
import { useUser } from 'context/UserContext'
import { addTranslation } from 'api/translation'
import { STORAGE_KEY_USER } from 'const/storageKeys'
import { storageSave } from 'utils/storage'
import { Box, Container } from '@mui/material'
import TranslationForm from 'components/Translation/TranslationForm'
import TranslationCard from 'components/Translation/TranslationCard'
import './Translation.css'

const style = {
	container: {
		marginTop: '4em',
	},
	box: {
		marginTop: '4em',
	},
	card: {
		height: '30em',
		boxShadow: 'rgba(0, 0, 0, 0.35) 0px 5px 15px',
	},
	cardActions: {
		backgroundColor: '#845EC2',
	},
	translateBtn: {
		color: 'black',
		backgroundColor: 'white',
		borderRadius: '5em',
	},
}

const Translation = () => {
	const [arrayWord, setArrayWord] = useState([])
	const { user, setUser } = useUser()

	const handleTranslationClicked = async (translationInput) => {
		const translation = translationInput
		console.log(translation)
		if (!translation) {
			alert('Please enter a word...')
			return
		}

		const [error, updatedUser] = await addTranslation(user, translationInput)
		if (error !== null) {
			return
		}

		// Keep UI state and Server state in sync
		storageSave(STORAGE_KEY_USER, updatedUser)
		// Update context state
		setUser(updatedUser)

		console.log('Error: ' + error);
		console.log('Result: ' + updatedUser);
	}


	return (
		<Container sx={style.container}>
			<Box sx={style.box}>
				<TranslationForm onTranslate={handleTranslationClicked} setArrayWord={setArrayWord} />
			</Box>
			<Box sx={style.box}>
				<TranslationCard arrayWord={arrayWord} />
			</Box>
		</Container>
	)
}

export default withAuth(Translation)
