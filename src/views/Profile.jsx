import React, { useEffect } from 'react'
import ProfileActions from 'components/Profile/ProfileActions'
import ProfileHeader from 'components/Profile/ProfileHeader'
import ProfileTranslationHistory from 'components/Profile/ProfileTranslationHistory'
import { useUser } from 'context/UserContext'

import withAuth from 'hoc/withAuth'
import { findUserById } from 'api/user'
import { storageSave } from 'utils/storage'
import { STORAGE_KEY_USER } from 'const/storageKeys'

const Profile = () => {
    const { user, setUser } = useUser()
    console.log(user)

    // useEffect(() => {
    //     const findUser = async () => {
    //         const [error, latestUser] = await findUserById(user.id)
    //         if (error === null) {
    //             storageSave(STORAGE_KEY_USER, latestUser)
    //             setUser(latestUser)
    //         }
    //     }
    //     // findUser()
    // }, [setUser, user.id])

    return (
        <>
            <ProfileHeader />
            <ProfileActions />
            <ProfileTranslationHistory translations={user.translations} user={user.username} />
        </>
    )
}

export default withAuth(Profile)