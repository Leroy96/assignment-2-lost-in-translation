import React from "react"
import { Container } from "@mui/material"
import LoginHeader from "components/Login/LoginHeader"
import LoginForm from "components/Login/LoginForm"

// Styles
const style = {
    loginform: {
        position: 'absolute',
    }
}

const Login = () => {
    return (
        <>
            <Container >
                <LoginHeader />
            </Container>
            <Container sx={style.loginform}>
                <LoginForm />
            </Container>
        </ >
    )

}

export default Login