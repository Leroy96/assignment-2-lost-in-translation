import { createHeaders } from './index'

const apiURL = process.env.REACT_APP_API_URL

export const addTranslation = async (user, translations) => {
	try {
		const response = await fetch(`${apiURL}/${user.id}`, {
			method: 'PATCH',
			headers: createHeaders(),
			body: JSON.stringify({
				translations: [...translations, translations],
			}),
		})
		if (!response.ok) {
			throw new Error('Could not update the translation')
		}
		const result = await response.json()
		console.log(result)
		return [null, result]
	} catch (error) {
		return [error.message, null]
	}
}

export const clearTranslationHistory = async (userId) => {
	try {
		const response = await fetch(`${apiURL}/${userId}`, {
			method: 'PATCH',
			headers: createHeaders(),
			body: JSON.stringify({
				translations: [],
			}),
		})
		if (!response.ok) {
			throw new Error('Could not update translations')
		}
		const result = response.json()
		return [null, result]
	} catch (error) {
		return [error.message, null]
	}
}
