import React from "react"

const { useUser } = require("context/UserContext")
const { Navigate } = require("react-router-dom")

const withAuth = Component => props => {
    // @ts-ignore
    const { user } = useUser()
    if (user !== null) {
        return <Component {...props} />
    } else {
        return < Navigate to='/' />
    }
}

export default withAuth