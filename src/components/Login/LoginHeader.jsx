import React from "react"
import { Box, Grid, Typography } from "@mui/material"
import { FontSize, MarginTool, SizeTool } from "components/helpers/css-helper"
// Images
import Logo from '../../assets/Logo.png'
import Splash from '../../assets/Splash.svg'

const style = {
    title: {
        color: 'white',
        fontFamily: 'Love Ya Like A Sister, cursive',
    },
}

const LoginHeader = () => {
    return (
        <>
            <Grid container spacing={2} sx={MarginTool(4, 0, 6, 0)}>
                <Grid item xs={4}>
                    <Box component='img' src={Logo} alt="Logo" sx={[{ position: 'fixed' }, MarginTool(2, 0, 0, 2), SizeTool(5, 14, 5, 14)]} />
                    <Box component='img' src={Splash} alt="Splash" sx={SizeTool(8, 22, 8, 22)} />
                </Grid>
                <Grid item xs={8}>
                    <Typography sx={[style.title, FontSize(5)]}>Lost in Translation</Typography >
                    <Typography sx={[style.title, FontSize(3)]}>Get started</Typography>
                </Grid>
            </Grid>
        </>
    )
}

export default LoginHeader