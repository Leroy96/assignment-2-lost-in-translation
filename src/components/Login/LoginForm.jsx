import React, { useEffect, useState } from "react"
import { Card, IconButton, Input } from "@mui/material"
import { ArrowCircleRight, KeyboardOutlined } from "@mui/icons-material"
import { BorderTool, MarginTool, SizeTool } from "components/helpers/css-helper"
import { useNavigate } from "react-router-dom"
import './LoginForm.css'

import { loginUser } from "../../api/user"
import { useForm } from "react-hook-form"
import { storageSave } from "utils/storage"
import { useUser } from "context/UserContext"
import { STORAGE_KEY_USER } from "const/storageKeys"


const style = {
    form: {
        width: '90%',
    },
    input: {
        fontSize: '6em',
    },
    card: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        height: '10em',
    },
}

const usernameConfig = {
    required: true,
    minLength: 3
}

const LoginForm = () => {
    // Hooks
    const { register, handleSubmit, formState: { errors } } = useForm()
    const { user, setUser } = useUser()

    // Local state
    const [loading, setLoading] = useState(false)
    const [apiError, setApiError] = useState(null)

    let navigate = useNavigate()

    // Side effects
    useEffect(() => {
        // apiFindAllUsers().then((users) => {
        //     setUsers(users)
        // }).catch((error) => { setError(error.message) }).finally(() => {
        //     setLoading(false)
        // })
        if (user !== null) {
            navigate('translate')
        }
        console.log('User has changed', user)

    }, [navigate, user])

    // const onSubmit = (event) => {
    //     event.preventDefault()
    //     setLoading(true)
    //     const { value } = usernameRef.current
    //     const enteredName = value
    //     if (enteredName !== '') {
    //         apiFindAllUsers().then((users) => {
    //             if (users.find(({ username }) => username === enteredName)) {
    //                 console.log('Username already taken.')
    //             } else {
    //                 apiCreateUser(enteredName).then(newUser => {
    //                     console.log('Created user: ', newUser)
    //                     dispatch(createUser(enteredName))
    //                     setLoggedIn(true)
    //                     routeChange('translate')
    //                 }).catch(error => setError("ERROR CREATING: " + error.message))
    //             }
    //         }).catch((error) => {
    //             setError(error.message)
    //         }).finally(() => {
    //             setLoading(false)
    //         })
    //     } else {
    //         alert('Enter username')
    //     }
    //     usernameRef.current.value = "";
    // }

    const onSubmit = async ({ username }) => {
        setLoading(true)
        const [error, userResponse] = await loginUser(username)
        if (error !== null) {
            setApiError(error)
        }
        if (userResponse !== null) {
            storageSave(STORAGE_KEY_USER, userResponse)
            setUser(userResponse)
        }
        setLoading(false)
    }

    const ArrowButton = () => (
        <IconButton type='submit' disabled={loading} >
            <ArrowCircleRight sx={[SizeTool(2, 5, 2, 5), MarginTool(0, 0, 0, 0), { color: '#845EC2', padding: 0 }]} />
        </IconButton>
    )

    const errorMessage = (() => {
        if (!errors.username) {
            return null
        }
        if (errors.username.type === 'required') {
            return <span style={MarginTool(0, 0, 0, 4.7)}>Username is required</span>
        }
        if (errors.username.type === 'minLength') {
            return <span style={MarginTool(0, 0, 0, 4.7)}>Username is too short</span>
        }
    })()

    return (
        <>
            <Card sx={[style.card, BorderTool(0, 0, 5, 0, 'solid', '#845EC2')]}>
                <form onSubmit={handleSubmit(onSubmit)} style={style.form}>
                    <fieldset className='fieldset'>
                        <label htmlFor="username"></label>
                        <Input name="username" id="username" placeholder="What's your name?" fullWidth disableUnderline
                            startAdornment={<KeyboardOutlined sx={MarginTool(0, 1, 0, 1)} />}
                            endAdornment={<ArrowButton />}
                            {...register('username', usernameConfig)}
                        />
                    </fieldset>
                    {loading && <p>Logging in...</p>}
                    {apiError && <p>{apiError}</p>}
                    {errorMessage}
                </form>
            </Card>
        </>
    )
}

export default LoginForm