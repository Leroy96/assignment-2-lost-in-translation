import * as React from 'react'
import { AppBar, Avatar, Container, Toolbar, Typography } from '@mui/material'
import { AccountCircle } from '@mui/icons-material'
import './Navbar.css'
import { useUser } from 'context/UserContext'
import { NavLink, useNavigate } from 'react-router-dom'

const style = {
    navbar: {
        backgroundColor: 'hsl(41, 92%, 64%)',
        borderBottom: '2px solid hsla(41, 52%, 42%, 0.349)',
    },
    title: {
        flexGrow: 1,
        fontFamily: 'Love Ya Like A Sister, cursive',
    },
    container: {
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    avatar: {
        height: '2em',
        width: '2em',
        backgroundColor: '#845EC2'
    },
    icon: {
        height: '1.5em',
        width: '1.5em',
    },
    nametag: {
        borderRadius: '5em',
        backgroundColor: '#845EC2',
        height: '1.8em',
        minWidth: '7em',
        margin: '0',
        textAlign: 'center'
    }
}

export default function Navbar() {
    const { user } = useUser()
    const navigate = useNavigate()

    const goToProfile = () => {
        navigate('profile')
    }

    return (
        <AppBar position="static" sx={style.navbar}>
            <Toolbar>
                <Container sx={style.container}>
                    <Typography variant="h5" component="div" sx={style.title}>
                        Lost in Translation
                    </Typography>
                    {user !== null
                        ? <div style={{ display: 'flex', alignItems: 'center', gap: '1em' }}>
                            <NavLink to='/translate'>Translate</NavLink>
                            <NavLink to='/profile'>Profile</NavLink>
                            <div onClick={goToProfile} style={{ display: 'flex', alignItems: 'center' }}>
                                <div
                                    // @ts-ignore
                                    style={style.nametag}>
                                    <span>{user.username}</span>
                                </div>
                                <Avatar sx={style.avatar}>
                                    <AccountCircle sx={style.icon} />
                                </Avatar>
                            </div>
                        </div>
                        : <></>}
                </Container>
            </Toolbar>
        </AppBar>
    );
}