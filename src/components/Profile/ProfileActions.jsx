import React from "react"
import { STORAGE_KEY_USER } from 'const/storageKeys'
import { storageDelete, storageSave } from 'utils/storage'
import { useUser } from "context/UserContext"
import { Button } from "@mui/material"
import { clearTranslationHistory } from "api/translation"
import { clear } from "@testing-library/user-event/dist/clear"

const ProfileActions = () => {
    const { user, setUser } = useUser()

    const handleLogoutClick = () => {
        if (window.confirm('Are you sure?')) {
            storageDelete(STORAGE_KEY_USER)
            setUser(null)
        }
    }

    const handleClearHistoryClick = async () => {
        if (window.confirm('Are you sure?\nThis can not be undone!')) {
            return
        }
        const [clearError] = await clearTranslationHistory(user.id)
        if (clearError !== null) {
            return
        }
        const updateUser = {
            ...user,
            translations: []
        }
        storageSave(updateUser)
        setUser(updateUser)
    }

    return (
        <ul>
            <Button variant='contained' sx={{ backgroundColor: '#845EC2' }} onClick={handleClearHistoryClick}>Clear History</Button>
            <Button variant='contained' color="error" onClick={handleLogoutClick}>Logout</Button>
        </ul>
    )
}

export default ProfileActions