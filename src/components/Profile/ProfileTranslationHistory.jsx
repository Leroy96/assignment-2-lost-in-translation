import { Container } from "@mui/material"
import React from "react"
import ProfileTranslationHistoryItem from "./ProfileTranslationHistoryItem"

const ProfileTranslationHistory = ({ translations, user }) => {

    const translationList = translations.map((translation, index) => <ProfileTranslationHistoryItem key={index + ' ' + translation} translation={translation} />)

    return (
        <>
            <Container>
                <h4>Hello {user}</h4>
                <ul>{translationList}</ul>
            </Container>
        </>
    )
}

export default ProfileTranslationHistory