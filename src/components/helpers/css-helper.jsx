/* eslint-disable no-useless-concat */

export function MarginTool(top, right, bottom, left) {
    return {
        marginTop: top + 'em',
        marginBottom: bottom + 'em',
        marginLeft: left + 'em',
        marginRight: right + 'em',
    }
}

export function SizeTool(minHeight, maxHeight, minWidth, maxWidth) {
    return {
        minHeight: minHeight + 'em',
        maxHeight: maxHeight + 'em',
        minWidth: minWidth + 'em',
        maxWidth: maxWidth + 'em',
    }
}

export function FontSize(size) {
    return {
        fontSize: size + 'em'
    }
}

export function BorderTool(top, right, bottom, left, type, color) {
    return {
        borderTop: top + 'px' + ' ' + type + ' ' + color,
        borderRight: right + 'px' + ' ' + type + ' ' + color,
        borderBottom: bottom + 'px' + ' ' + type + ' ' + color,
        borderLeft: left + 'px' + ' ' + type + ' ' + color,
    }
}