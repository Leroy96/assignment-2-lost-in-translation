import React from "react";
import { Box, Button, Card, CardActions, CardContent } from "@mui/material";
import { BorderTool } from "components/helpers/css-helper";
import { NavLink } from "react-router-dom";
import TranslationImage from "./TranslationImage";

const style = {
    card: {
        height: '30em',
        boxShadow: 'rgba(0, 0, 0, 0.35) 0px 5px 15px',
    },
    cardActions: {
        backgroundColor: '#845EC2',
    },
    translateBtn: {
        color: 'black',
        backgroundColor: 'white',
        borderRadius: '5em'
    }
}

const TranslationCard = ({ arrayWord }) => {
    return (
        <Card sx={[style.card, BorderTool(1, 1, 4, 1, 'solid', '#845EC2')]}>
            <CardContent>
                <Box>

                    {arrayWord.map((letter, index) => (
                        <TranslationImage key={index + ' ' + letter} name='sign' letter={letter}></TranslationImage>
                    ))}
                </Box>
            </CardContent>
            <CardActions sx={style.cardActions}>
                <NavLink to='/profile'><Button variant='contained' sx={style.translateBtn} >Translation History</Button></NavLink>
            </CardActions>
        </Card>
    )
}

export default TranslationCard