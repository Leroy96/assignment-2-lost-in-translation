import React, { useState } from "react";
import './TranslationForm.css'
import { KeyboardOutlined } from "@mui/icons-material"
import { Button, Input } from "@mui/material"
import { MarginTool } from "components/helpers/css-helper"
import { useForm } from 'react-hook-form'

const style = {
    form: {
        width: '100%',
    },
    input: {
        fontSize: '6em',
    },
    translateBtn: {
        color: 'white',
        backgroundColor: '#845EC2',
        borderRadius: '5em',
        margin: '1em',
        paddingLeft: '1.2em',
        paddingRight: '1.2em'
    }
}

const TranslationForm = ({ onTranslate, setArrayWord }) => {
    const { register, handleSubmit } = useForm()
    const [loading, setLoading] = useState(false)
    const [error, setError] = useState('')

    const [word, setWord] = useState('')

    const onSubmit = ({ translate }) => {
        setLoading(true)
        const lowerCaseWord = translate.toLowerCase().trim()
        const splitWord = lowerCaseWord.split('')
        setWord(lowerCaseWord)
        setArrayWord(splitWord)
        onTranslate(lowerCaseWord)
        setLoading(false)
    }

    const ArrowButton = () => (
        <Button type='submit' onClick={onTranslate} variant='contained' sx={style.translateBtn}>{loading === true ? <>Loading</> : <>Translate</>}</Button>
    )

    return (
        <form onSubmit={handleSubmit(onSubmit)} style={style.form}>
            <fieldset className='fieldset'>
                <label htmlFor="translate"></label>
                <Input placeholder="Enter text to translate..." fullWidth disableUnderline
                    startAdornment={<KeyboardOutlined sx={MarginTool(0, 1, 0, 1)} />}
                    endAdornment={<ArrowButton />}
                    {...register('translate')}
                />
            </fieldset>

        </form >
    )
}

export default TranslationForm