import React from "react"

const TranslationImage = ({ name, letter }) => {
    return (
        <img src={`img/${letter}.png`} alt={name} width='55' />
    )
}

export default TranslationImage