import React from 'react';
import Navbar from './components/Navbar/Navbar';
import Login from './views/Login';
import Translation from './views/Translation';
import Profile from 'views/Profile';
import { Container, Paper } from "@mui/material"
import { BrowserRouter, Route, Routes } from 'react-router-dom';

const background = {
  backgroundColor: ' hsl(41, 92%, 64%)',
  height: '35em',
}

const App = () => {
  console.log('APP RENDER');
  return (
    <BrowserRouter>
      <Paper sx={background}>
        <Navbar />
        <Container>
          <Routes>
            <Route path='/' element={<Login />} />
            <Route path='/translate' element={<Translation />} />
            <Route path='/profile' element={<Profile />} />
          </Routes>
        </Container>
      </Paper>
    </BrowserRouter>
  );
}

export default App;
